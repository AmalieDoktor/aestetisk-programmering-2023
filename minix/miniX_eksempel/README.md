# Et eksempel på en miniX
Dette er et eksempel på en miniX aflevering.

Her er et screenshot af mit program:

![](/minix/miniX_eksempel/screenshot.png)

Og her er et link til at køre mit program:

https://margretelr.gitlab.io/aestetisk-programmering-2023/minix/miniX_eksempel/eksempel-sketch/index.html

# Beskrivelse af programmet
Mit program tegner nogle farverige firekanter med random skiftende længder ovenpå hinanden. Når længderne lander over et bestemt antal, så beder jeg programmet tegne de næste firekanter længere nede. Og sådan bliver den ved indtil den når udover canvaset, hvor den bliver bedt om at starte ved toppen igen. På den måde opnår jeg et billede skiftende billede af flotte firekanter.

# En interessant oplevelse
Da jeg lige startede med at bede programmet om at tegne firekanter, gjorde den det i et vild hurtig tempo, så man fik næsten epilepsi af det. Så for at gøre koden langsommere, satte jeg framerate lavere inde i setup funktionen:
```
function setup() {
  
  createCanvas(500, 500); //lav et canvas på 500x500 pixels
  background(255, 220, 220); //gør baggrunden for canvas lyderød
  frameRate(5); //sætter et lavere framerate, så man ikke får et epileptisk anfald :P

}
```

# Refleksioner omkring programmet
Jeg gættede mig ret meget frem til farverne - altså jeg testede bare en masse tal af, og så hvordan det så ud på canvasset. Det er interessant hvordan maskinen læser farver som RGB værdier, hvilket betyder ingenting for mig. Når vi maler et billede IRL kan jeg hele tiden se malingens farve og nemt blande det med andre farver. Men fordi maskinen snakker et andet sprog, er jeg nødt til at bruge flere tankekræfter på at komme frem til den farve, jeg gerne vil have: Hvis farven skal være lysere, skal værdierne generelt være højere. Hvis farven skal være mere pang, skal forskellen i værdierne være størrer osv.

# Opgraderingsmuligheder
Det kunne være nice, hvis firekanterne starter på random x værdier også. Og jeg gad også godt have en knap, så man kan downloade et screenshot af sit billede. Måske kunne programmet også bruges til at generere en random farvepalette til en?
