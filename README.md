# ÆSTETISK PROGRAMMERING (20 ECTS) F2023

Underviser: Margrete Lodahl Rolighed (margretelr@cc.au.dk)<br>
Medunderviser: Anders Visti (andersvisti@gmail.com)<br>
Instruktorer: Trine Rye Østergaard (202108625@post.au.dk) og Mikkel Aarup Vinther (202105054@post.au.dk)<br>

##### 📒 Grundbog: 
-	Soon Winnie & Cox, Geoff, Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020 <br>
PDF her: http://www.openhumanitiespress.org/books/titles/aesthetic-programming/ <br>
Online her: https://aesthetic-programming.net/ <br>
Eksempelkoder fra bogen: https://aesthetic-programming.gitlab.io/book/<br>

## Undervisning
Mandage kl. 10 - 13 @Lokale 5361 - 135 <br>
Onsdage kl. 8 - 11 @Lokale 5361 - 144 <br>
Instruktortimer torsdage kl. 8 - 10 @Lokale 5361 – 144 <br>

Fællesnoter for hele klassen (Variapad): https://pad.vvvvvvaria.org/æp23

#### Andre aktiviteter:
Shutup-and-code sessioner fredage kl. 13 - 15 @Lokale 5361 - 135 <br>

Enkelte fredage erstattes shutup-and-code med gæsteforelæsninger eller workshops. <br>

[Digital Design Lab](https://www.facebook.com/AUDDLab) står også til rådighed for at hjælpe i deres åbningstider.

## Kort om kurset
[Æstetisk programmering](https://kursuskatalog.au.dk/en/course/115561/Aesthetic-programming) giver en introduktion til programmering på samme tid med at udforske forholdet mellem kode og kulturelle/æstetiske fænomener i tætknytning med feltet software studies. I kurset vil programmering fungere som en praksis hvorudfra vi kan beskrive og konceptualisere verdenen omkring os, og dermed begynde at forstå de strukturer og systemer, der er i den moderne digitale kultur. Vi vil primært bruge programmeringssproget [p5.js](https://p5js.org/), som danner grundlag for kommende kurser i Digital Design uddannelsen.

Kursets formål er, at de studerende lærer at designe og programmerer et stykke software, ikke blot for at forstå den pragmatiske funktion af kodning, men ydermere for at kunne forstå programmering som en refleksiv, æstetisk og kritisk handling. Det er på denne baggrund de studerende får evnen til at kunne analysere, diskutere og vurdere kode.

## Faglige kompetencer
Den studerende lærer at…
1.	Læse og skrive kode i et givet programmeringssprog
2.	Bruge programmering til at udforske digital kultur
3.	Forstå programmering som en æstetisk og kritisk handling udover den pragmatiske funktion af kode

## MiniX - Ugentlige programmerings øvelser
🚪 [Gå til overblik over alle øvelserne](../main/minix) (lokaliseret i denne repository > minix)


Hver uge vil I blive bedt om at aflevere en lille programmerings øvelse, som vi kalder miniX (mini exercise).

Deadline for en miniX er om søndagen efter den pågældende uges undervisning.

#### 💻 En miniX aflevering består af: 
- **RUNME**: et stykke software program, som indeholde de nødvendige js biblioteker, en HTML fil, din .js sketch fil og et link, hvorpå koden virker online.

- **README**: en bid tekst, som beskriver programmet; et screenshot eller video af det færdige produkt; tekst om de refleksioner, som koden og øvelsen har skabt; og besvarelse af de spørgsmål, som opgaven har stillet. 
     - [Markdown formattering guides for README kan findes her](https://about.gitlab.com/handbook/markdown-guide/)
     - [Markdown Cheatsheet](https://gitlab.com/francoisjacquet/rosariosis/-/wikis/Markdown-Cheatsheet)

Alt dette udgør en miniX, og afleveres på jeres GitLab profiler. Nogle miniX er individuelle øvelser, og nogle vil være gruppeøvelser.

#### ✍️ Peer-feedback på miniX
[🚪 Gå til CryptPad oversigt over alles GitLab links og feedback organisering](https://cryptpad.fr/sheet/#/2/sheet/edit/zGFZj7Xg-FcgjYXXMzD1HObJ/)

Der ligger rigtig meget læring i at lave jeres miniX, men også fra at læse og diskutere hinandens kode. Derfor skal I bruge noget tid på at give hinanden peer-feedback på miniX. <br>
Når en miniX er afleveret og offentliggjort om søndagen, bliver der givet peer-feedback fra de andre studerende og underviser/instruktorer igennem GitLabs ’Issues’ funktion. Deadline for peer-feedback er onsdage inden undervisningen.
1.	Gå til vedkommendes GitLab repository, og find ”Issues” i navigationsbjælken
2.	Klik på ”New Issue”
3.	Skriv din feedback med titlen ”Feedback på miniX[**nr**] af [**Dit fulde navn**]”

#### 💬 Præsentation af miniX

Jeres miniX er både en stor videnskilde til jer selv individuelt, men også til klassen som kollektiv. Derfor vil I løbende i kurset skiftes til at præsentere jeres miniX for klassen. I kan finde jeres præsentationsgang i [semesterplanen](../Semesterplan.md).

Hvis du har lavet din miniX,  bør dette ikke være en ekstra arbejdsbyrde, for du har allerede det materiale, som skal fremlægges. Brug derfor ikke en ekstra dag på at forberede dig.

Præsentationen varer **15 minutter**, hvor du:
- Demonstrerer dit software, kører din kode, præsentere dine ideer og processen, og artikulere det, som er i din README i lyset af teorier og koncepter fra litteraturen.

- Vis og fortæl os om en fejl eller en bug, som du oplevede under kodningen (uanset hvor trivielle de er) - fx hvordan opdagede du den, og hvordan fixede du den eller lavede en workaround?

- Hvis du har udforsket nye funktioner eller biblioteker, vil det være godt at dele det med klassen, og vise andre hvordan du har brugt dem i dit program. 

- Forberede et spørgsmål til klassen, som kan starte en diskussion.

Resten af klassen [tager fællesnoter for præsentationen på Variapad](https://pad.vvvvvvaria.org/æp23).

## Shutup-and-code sessioner
Hver fredag er der mulighed for at komme til en slags ’kode-café’, hvor I kan få hjælp af hinanden eller af jeres instruktorer til at klare jeres miniX eller andre programmeringsproblemer. 

Vi kalder det _shutup-and-code_ for at skabe et rum og tidsrum, hvor man kan få lov at være fokuseret på sin programmeringspraksis, og forhåbentlig komme i et godt flow og godt igang med sin miniX.<br>

OBS: Hvis man fx er forhindret i at komme til shutup-and-code sessionerne, er det også altid muligt at besøge [DDLab](https://www.facebook.com/AUDDLab) i deres åbningstid, hvor kompetente lab-ansatte også står til rådighed.

## Programmeringsmiljø
- **Programmeringssprog**: hovedsagligt [p5.js](https://p5js.org/) + lidt HTML og CSS
- **Code editor**: [Visual Studio Code](https://code.visualstudio.com/). Det er herfra vi kan åbne vores projekter på computeren og redigere i koden.
     - Med extensionen _Live Server_ kan vi åbne en lokal port og se et 'preview' af koden live.
- **Development platform**: [GitLab](https://gitlab.com/). Her kan vi dele vores kode med hinanden og give feedback.

## Forventninger og forudsætningskrav for at gå til ordinær eksamen
Følgende er kravene, som skal opfyldes, før man kan gå til ordinær eksamen:
1.	Læs litteraturen og se videoerne jf. semesterplanen inden undervisningen starter.
2.	Aktiv deltagelse i undervisningen og instruktortimernes diskussioner og øvelser, præsentere miniX og give peer-feedback. 
3.	Aflevere den ugentlige miniX til tiden.
4.	Aflevere den ugentlige peer-feedback på andres miniX til tiden.
5.	Præsentere og aflevere det endelige gruppeprojekt. 

OBS: 20 ECTS svarer ca. til 25 timer om ugen inklusiv undervisning og instruktortimer

## Eksamen: 19/6 – 22/6
OBS: Dato for eksamensaflevering kan godt ændre sig. [Hold jer opdateret om eksamensdatoer her](https://studerende.au.dk/studier/fagportaler/arts/eksamen/eksamensplaner/145102-202-302-digital-design).

Eksamenen er en mundtlig eksamen, hvori den individuelle studerende præsenterer, analysere og reflektere over det endelige projekt. 

Eksamen varer 30 minutter, hvoraf den studerende præsenterer i 10 minutter, efterfulgt af 12 minutters diskussion, og slutteligt 8 minutter til votering.

Emnet skal være relevant i forhold til fagets indhold, og bedømmelsen sker på baggrund af den mundtlige præsentation alene.

**Reeksamen**

Forudsætningskrav: Aflevere miniX 1-8 og det endelige projekt, som udføres individuelt.


## Klassemiljø og -protokol
1.	🧡 **Code of conduct (fra p5.js community statement):**<br>
We are a community of, and in solidarity with, people from every gender identity and expression, sexual orientation, race, ethnicity, language, neuro-type, size, ability, class, religion, culture, subculture, political opinion, age, skill level, occupation and background.

2.	📝 **Papir og blyant:**<br>
Selvom vi har med skærmen og kodning at gøre, så er det altid en god idé at medbringe papir eller en notesbog, da det kan hjælpe med at sketche og prototype dine ideer og strukturere dine tanker og forståelser.

3.	✌️ **To-før-mig politik:**<br>
Når du oplever en fejl i dit program, skal du først søge hjælp hos to af dine medstuderende før du henvender dig til underviseren for hjælp. På den måde øver du dig i at nedbryde dit spørgsmål og faciliterer peer-learning, hvor I lærer fra hinanden.

## Inspirationskilder:
- Twitter profil: [Daglige sketches i Processing af Saskia Freeke](https://twitter.com/sasj_nl) 
     - Se hendes talk [her: Making Everyday - GROW 2018](https://www.youtube.com/watch?v=nBtGpEZ0-EQ&fbclid=IwAR119xLXt4nNiqpimIMWBlFHz9gJNdJyUgNwreRhIbdJMPPVx6tq7krd0ww)
- Twitter profil: [Zach Lieberman](https://twitter.com/zachlieberman)
- Facebookgruppe: [Creative Coding with Processing and P5.js
](https://www.facebook.com/groups/creativecodingp5/)
- [OpenProcessing](https://www.openprocessing.org/) (søg med keywords)

## Andre referencer:
- [JavaScript basics](https://github.com/processing/p5.js/wiki/JavaScript-basics) af p5.js
- [NEM Programmering](https://www.nemprogrammering.dk/) - lavet af studerende
- [_Coding Projects with p5.js_ af Catherine Leung](https://cathyatseneca.gitbooks.io/coding-projects-with-p5-js/)
- [Video: _Crockford on Javascript_ (mere historisk kontekst)](https://www.youtube.com/watch?v=JxAXlJEmNMg&list=PLK2r_jmNshM9o-62zTR2toxyRlzrBsSL2)
- McCarthy, L, Reas, C & Fry, B. *Getting Started with p5.js: Making interactive graphics in Javascript and Processing (Make)*, Maker Media, Inc, 2015.
- Shiffman, D. *Learning Processing: A Beginner’s Guide to Programming Images, Animations, and Interaction*, Morgan Kaufmann 2015 (Second Edition)
- Haverbeke,M. *[Eloquent JavaScript](https://eloquentjavascript.net/)*, 2018.
- Youtube playlist: [p5.js Tutorials af Daniel Shiffman](https://www.youtube.com/playlist?list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA)
- Youtube playlist: [Intro to Programming in p5.js af Xin Xin](https://www.youtube.com/watch?v=wiG7wLwyW0E&list=PLT233rQkMw761t_nQ_6GkejNT1g3Ew4PU)





